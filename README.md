# Font adjustment for WSL personal webpage


## Getting started
This short tutorial aims to elaborate how to adjust the text font in WSL personal webpage. Hopefully, it can help the current group members and the future as well.
For the main reference of this tutorial, see: https://blog.hubspot.com/website/change-font-in-html.

## How to do
Before the start of adjustment, it is assumed that one can get access to the editing page of a section (e.g., Research, Education...).
Then,
- Go to the editing page of a section (e.g., Research);
- Go to *source code (Quellcode)*; font adjustment starts from here;
- For a paragrah that you want to modify the font overall, put    **<span& style="font-size: 17px">**   (!!!delet &)at the begin of the paragraph indicating that target modification starts from the begin and    **<&/span>**  (!!!delet &) at the end to terminate the modification;
- For a sentence or part of a paragraph, it works simialr. Put two commands mentioned above in the begin and end with the part you want to modify in the middle;
- After adding command codes, **do not leave the source code**, just scroll up and click *save on computer (Speichern)*. Next, refresh the personal web page, the font of the targeted sentences should already be modified;

**Note**
- 17px is the targeted font to be adjusted to. The default font for WSL webpage seems to be around 17-18px. 

**Hint**
- As font ajustment is not graphically available for WSL personal webpage.Therefore, such modification from the source code should be the last step after all texts are ready in place. Otherwise, after resaving the editing page, the source code would be automatically undone with previous modification disappeared.


## Contact
liangzhi.chen@wsl.ch

## Author(s)
Liangzhi Chen, Dynamic Macroecology/Land Change Science/WSL
